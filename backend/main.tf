provider "aws" {
  region = "eu-west-2"
}

module "backend" {
    source = "git@gitlab.com:MarshallT8/terraform-module-backend.git"
    TFstate_bucket_name = "${var.TFstate_bucket_name}"
    dynamodb_table_name = "${var.dynamodb_table_name}"
}

