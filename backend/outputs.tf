output "TFstate_bucket_name" {
  value       = module.backend.TFstate_bucket_name
  description = "The name of the S3 bucket"
}
output "TFstate_bucket_arn" {
  value       = module.backend.TFstate_bucket_arn
  description = "The arn of the S3 bucket"
}
output "dynamodb_table_name" {
  value       = module.backend.dynamodb_table_name
  description = "The name of the DynamoDB table"
}