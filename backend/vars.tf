variable "TFstate_bucket_name" {
  description = "name of private bucket to host TFstate file"
  type = string
  default = "wr-tfstate"
}
variable "dynamodb_table_name" {
  description = "name of dynamodb table for locks e.g xxxx-tfstate-locks"
  type = string
  default = "wr-tfstate-locks"
}