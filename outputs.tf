output "s3_bucket_name" {
  value       = module.svrless.s3_bucket_name
  description = "The name of the S3 bucket"
}
output "s3_bucket_arn" {
  value       = module.svrless.s3_bucket_arn
  description = "The arn of the S3 bucket"
}
output "s3_bucket_endpoint" {
  value       = module.svrless.s3_bucket_endpoint
  description = "The endpoint for accessing the S3 bucket"
}
output "cog_usrpool_id" {
  value       = module.svrless.cog_usrpool_id
  description = "The id of the Cognito user sign-in pool"
}
output "cog_usrpool_arn" {
  value       = module.svrless.cog_usrpool_arn
  description = "The arn of the Cognito user sign-in pool"
}
output "cog_usrpool_clientid" {
  value       = module.svrless.cog_usrpool_clientid
  description = "The id of the Cognito user sign-in client"
}
output "dynamodb_arn" {
  value       = module.svrless.dynamodb_arn
  description = "The arn of the DynamoDB table"
}
output "API_gateway_invoke_url" {
  value       = module.svrless.API_gateway_invoke_url
  description = "The invoke URL of the API"
}