provider "aws" {
  region = "eu-west-2"
}
module "svrless" {
    source = "git@gitlab.com:MarshallT8/terraform-module-svrless-webapp.git"
    cognito_userpool_name = "wr-cog-usrpool"
    cognito_client_name = "wr-cog-client"
    s3_bucket_name = "wildrides-mt42"
    dynamodb_table_name = "Rides"
    aws_iam_role_name = "WildRydesLambda"
    lambda_function_name = "RequestUnicorn"
    api_gateway_name = "WildRydes"
}